#!/usr/bin/env python3

from setuptools import find_packages, setup

if __name__ == "__main__":
    setup(
        name="untrac",
        version="0.0.1",
        license="BSD-2-Clause",
        description="",
        author="Alexander Færøy",
        author_email="ahf@0x90.dk",
        url="https://gitlab.torproject.org/ahf/untrac",
        packages=find_packages("src"),
        package_dir={"": "src"},
        include_package_data=True,
        install_requires=["Click"],
        tests_require=[],
        classifiers=[],
        entry_points="""
            [console_scripts]
            untrac=untrac.cli:main
        """,
    )
