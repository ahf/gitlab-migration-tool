# Copyright (c) 2019-2020 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import logging
import os
import os.path
import xmlrpc.client

from multiprocessing import Pool
from pprint import pformat
from typing import List, Optional


def empty_to_none(x: str) -> Optional[str]:
    value = x.strip()

    if value == "":
        return None

    return value

def listify(x: str, split: str) -> List[str]:
    if x == "":
        return []

    return list(map(lambda v: v.strip(), x.split(split)))

class TracClient:
    def __init__(self, url: str, username: str, password: str) -> None:
        self._proxy = xmlrpc.client.ServerProxy(url.format(**{
            "username": username,
            "password": password,
        }), use_builtin_types=True)

    def fetch_attachment(self, ticket_id, filename):
        saved_filename = "data/attachments/{}/{}".format(ticket_id, filename)

        if os.path.exists(saved_filename):
            return

        logging.info("Fetching Trac attachment from #{}: {} -> {}".format(ticket_id, filename, saved_filename))

        os.makedirs("data/attachments/{}/".format(ticket_id), exist_ok=True)
        content = self._proxy.ticket.getAttachment(ticket_id, filename)

        with open(saved_filename, "bw") as f:
            f.write(content)


    def fetch_ticket(self, ticket_id: int) -> None:
        logging.info("Fetching Trac ticket #{}".format(ticket_id))
        _ticket_id, _created, _updated, ticket = self._proxy.ticket.get(ticket_id)

        changelog = []
        for timestamp, username, event_type, old_value, new_value, extra in self._proxy.ticket.changeLog(ticket_id):
            changelog.append({
                "timestamp": timestamp,
                "username":  username,
                "type": event_type,
                "old_value": old_value,
                "new_value": new_value,
                "extra": extra,
            })

        attachments = []
        for filename, description, size, date, uploader in  self._proxy.ticket.listAttachments(ticket_id):
            attachments.append({
                "filename": filename,
                "description": description,
                "size": size,
                "date": date,
                "uploader": uploader,
            })

        metadata = {
            "last_modified": ticket["changetime"],
            "opened":        ticket["time"],                                # V X

            "reporter":      ticket["reporter"],                            # V X M
            "type":          ticket["type"],                                # V X M
            "summary":       ticket["summary"],                             # V X M (title)

            "status":      ticket.get("status", ""),                        # V X M
            "description": ticket.get("description", ""),                   # V X M (FIXME: Encoding of attachments)

            "keywords":    listify(ticket.get("keywords", ""), ","),        # V X M
            "cc":          listify(ticket.get("cc", ""), ","),              # V X M
            "reviewer":    listify(ticket.get("reviewer", ""), ","),        # V X M  (FIXME: ahf, reviewer::{nickname})

            # Some of these either returns None or the empty string.
            "component":     empty_to_none(ticket.get("component", "")),    # V X M
            "points":        empty_to_none(ticket.get("points", "")),       # V X M
            "actual_points": empty_to_none(ticket.get("actualpoints", "")), # V X M
            "version":       empty_to_none(ticket.get("version", "")),      # V X M
            "owner":         empty_to_none(ticket.get("owner", "")),        # V X M
            "milestone":     empty_to_none(ticket.get("milestone", "")),    # V X M
            "parent":        empty_to_none(ticket.get("parent", "")),       # V
            "resolution":    empty_to_none(ticket.get("resolution", "")),   # V X M
            "priority":      empty_to_none(ticket.get("priority", "")),     # V X M
            "severity":      empty_to_none(ticket.get("severity", "")),     # V X M
            "sponsor":       empty_to_none(ticket.get("sponsor", "")),      # V X M

            "attachments":  attachments,                                    # V X

            "changes":      changelog,                                      # V X

            "raw": ticket,
        }

        return metadata

    def fetch_ticket_range(self, start, end) -> None:
        for i in range(start, end):
            print(i)
            print(self.fetch_ticket(i))

    def methods(self) -> None:
        for method in self._proxy.system.listMethods():
            logging.info("Method: {}".format(method))

    def wiki_pages(self) -> List[str]:
        return self._proxy.wiki.getAllPages()

    def fetch_wiki_page_info(self, page: str) -> None:
        logging.info("Requesting wiki page info for {}".format(page))

        return self._proxy.wiki.getPageInfo(page)

    def fetch_wiki_page(self, page: str, version: int) -> None:
        logging.info("Requesting wiki page {} (version {})".format(page, version))

        return self._proxy.wiki.getPageVersion(page, version)

    def wiki_attachments(self, page: str):
        logging.info("Requesting attachment information from wiki page {}".format(page))
        return self._proxy.wiki.listAttachments(page)

    def fetch_wiki_attachment(self, attachment: str):
        logging.info("Requesting wiki attachment {}".format(attachment))
        return self._proxy.wiki.getAttachment(attachment)
