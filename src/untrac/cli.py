# Copyright (c) 2019-2020 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import click
import logging
import datetime
import os.path
import os
import gitlab
import pprint
import itertools
import sys

import yaml

from gitlab.exceptions import GitlabHttpError, GitlabCreateError

from .config import Config
from .trac import TracClient
from .db import Database
from .trac2down import convert

def tor_aliases():
    return [
        ("ln5", "linus"),
        ("Sebastian", "sebastian"),
        ("hellais", "art"),
        ("arlolra", "arlo"),
        ("pospeselr", "richard"),
        ("ggus", "gus"),
        ("mrphs", "nima"),
        ("saint", "griffin"),
        ("sjmurdoch", "sjm217"),
        ("phoul", "colin"),
        ("anadahz", "andz"),
        ("chelseakomlo", "chelsea"),
        ("nickm_mobile", "nickm"),
        ("kat5", "kat"),

        # Roger / Nick list.
        ("Dr_Whax", "DrWhax"),
        ("Jesse V.", "kernelcorn"),
        ("StrangeCharm", "flamsmark"),
        ("Sue Gardner", "spg"),
        ("T(A)ILS developers", "tails-developers"),
        ("ailanthus", "kate"),
        ("damecore", "kbergeron"),
        ("haviah", "hiviah"),
        ("inf0", "sina"),
        ("lnl", "linda"),
        ("mwenge", "hoganrobert"),
        ("notirl", "irl"),
        ("phobos", "andrew"),
        ("t0mmy", "tommyc"),
        ("virgilgriffith", "virgil"),
        ("zyan", "yan"),
        ("mo", "moritz"),
        ("Jaruga", "jaruga"),
        ("NickHopper", "nickhopper"),
        ("Shondoit", "shondoit"),
        ("Julius", "julius"),

        # The order of these two matters:
        ("micahlee", "micah"),
        ("micah", "hacim"),

        ("federico3", "federico"),

        ("cYphErPUNKs",   "cypherpunks"),
        ("cypherpunks2",  "cypherpunks"),
        ("cypherpunks3",  "cypherpunks"),
        ("cypherpunks33", "cypherpunks"),
        ("cypherpunks63", "cypherpunks"),
    ]


@click.group()
@click.option("--config-file", default="config.ini", help="Config file.")
@click.pass_context
def cli(context, config_file):
    context.ensure_object(dict)

    # Read our config file.
    config = Config(config_file)

    # Configure our run-time logger.
    logging.basicConfig(level=config.log_level(),
                        format="%(asctime)s %(levelname)s: %(message)s",
                        datefmt="%Y/%m/%d %H:%M:%S",)

    # Our database.
    database = Database(config.database())

    # Our Trac Client.
    trac_client = TracClient(config.trac_url(),
                             config.trac_username(),
                             config.trac_password())

    # Our Gitlab Client.
    gitlab_client = gitlab.Gitlab(config.gitlab_url(), private_token=config.gitlab_private_token(), api_version=4)

    # Store state for other commands to use.
    context.obj["config"] = config
    context.obj["trac_client"] = trac_client
    context.obj["database"] = database
    context.obj["gitlab_client"] = gitlab_client

@cli.command()
@click.pass_context
def stats(context):
    database = context.obj["database"]

    components = {}

    for ticket_id, json in database.tickets():
        component = json["component"]

        if component not in components:
            components[component] = set()

        components[component].add(ticket_id)

    for component, items in sorted(components.items(), key=lambda x: len(x[1]), reverse=True):
        print("{};{}".format(component, len(items)))

@cli.command()
@click.pass_context
@click.argument("filename")
def move_browser_tickets(context, filename):
    database = context.obj["database"]
    gitlab_client = context.obj["gitlab_client"]

    targets = {}

    with open(filename) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)

        for project in data:
            target  = project["target"]
            tickets = project["tickets"]
            assert target not in targets

            targets[target] = set(tickets)

    for target_a, tickets_a in targets.items():
        for target_b, tickets_b in targets.items():
            if target_a == target_b:
                continue

            for ticket in tickets_a:
                if ticket in tickets_b:
                    assert False

    # Find TracBot user.
    tracbot = gitlab_client.users.list(username="TracBot")[0]

    gitlab_users = {}

    for u in gitlab_client.users.list(all=True):
        gitlab_users[u.username] = u

    aliases = tor_aliases()

    for alias, ldap in aliases:
        # print("{} alias {}".format(alias, ldap))
        if alias != "micah":
            assert alias not in gitlab_users

        gitlab_users[alias] = gitlab_users[ldap]

    # federico on trac is not federico on ldap
    del gitlab_users["federico"]

    # Check what happens
    gitlab_projects = {}
    for project in gitlab_client.projects.list(all=True):
        assert project.path_with_namespace not in gitlab_projects
        gitlab_projects[project.path_with_namespace] = project

    gitlab_groups = {}
    for group in gitlab_client.groups.list(all=True):
        assert group.full_path not in gitlab_groups
        gitlab_groups[group.full_path] = group

    found_tickets = False

    # Make sure nobody have any tickets.
    # XXX AHF
    if True:
        for project_path in targets.keys():
            if project_path not in gitlab_projects:
                print("Need to create: {}".format(project_path))

                tokens = project_path.split("/")

                project_name = tokens[-1]
                group_name   = "/".join(tokens[0:len(tokens) - 1])

                group = gitlab_groups.get(group_name, None)

                if group is None:
                    print("Group '{}' missing".format(group_name))
                    sys.exit(1)

                logging.info("Creating project: {}".format(project_name))
                project = gitlab_client.projects.create({
                    "name": project_name,
                    "issues_enabled": True,
                    "emails_disabled": True,
                    "namespace_id": group.id,
                    "visibility": "public",
                    "merge_requests_enabled": False,
                    "snippets_enabled": False,
                }, sudo="tracbot")

                gitlab_projects[project_path] = project

            project = gitlab_projects[project_path]
            issues = project.issues.list(all=True)
            # issues = []

            if len(issues) > 0:
                found_tickets = True
                print("{}: {}".format(project_path, len(issues)))
            else:
                print("{}: N/A".format(project_path))

    if found_tickets:
        print("Projects have tickets... Giving up")
        # sys.exit(1)

    # We do a pass to figure out which labels and milestones we need to create in each new project.
    project_metadata = {}
    ignored_tickets_sum = 0
    ticket_map = {}

    for ticket_id, json in database.tickets():
        labels = set()

        ticket_map[ticket_id] = json

        # Handle component
        if json["component"] is not None:
            labels.add("component::{}".format(json["component"].lower()))

        if json["component"] != "Applications/Tor Browser":
            continue

        # Figure out which set it belongs to.
        project_target_name = None

        for target, tickets in targets.items():
            if ticket_id in tickets:
                assert project_target_name is None
                project_target_name = target

        if project_target_name is None:
            project_target_name = "tpo/applications/tor-browser"

        if project_target_name not in project_metadata:
            project_metadata[project_target_name] = {
                "tickets": set([]),
                "milestones": set([]),
                "labels": set([]),
            }

        project_metadata[project_target_name]["tickets"].add(ticket_id)

        # Handle Type.
        assert json["type"] in ["defect", "enhancement", "project", "task"]
        labels.add("type::{}".format(json["type"]))

        # Handle milestone.
        milestone_value = json["milestone"]

        if milestone_value is not None:
            labels.add("milestone::{}".format(milestone_value))
            project_metadata[project_target_name]["milestones"].add(milestone_value)

        # Handle version.
        if json["version"] is not None:
            labels.add("version::{}".format(json["version"].replace(":", "").lower()))

        # Handle priority.
        if json["priority"] is not None:
            labels.add("priority::{}".format(json["priority"].lower()))

        # Handle severity.
        if json["severity"] is not None:
            labels.add("severity::{}".format(json["severity"].lower()))

        # Handle sponsor.
        if json["sponsor"] is not None:
            labels.add("sponsor::{}".format(json["sponsor"][len("Sponsor"):]))

        # Handle points and actual points.
        if json["points"] is not None:
            labels.add("points::{}".format(json["points"]))

        if json["actual_points"] is not None:
            labels.add("actualpoints::{}".format(json["actual_points"]))

        # Handle owner.
        owners = set()

        if json["owner"] is not None:
            for owner in set(list(map(lambda x: x.strip(), json["owner"].split(",")))):
                labels.add("owner::{}".format(owner))

        # Handle reviewer.
        reviewers = set()

        if json["reviewer"]:
            for reviewer in set(json["reviewer"]):
                if reviewer in gitlab_users:
                    reviewers.add(gitlab_users[reviewer])

        # For now we implement reviewer as labels.
        for reviewer in reviewers:
            labels.add("reviewer::{}".format(reviewer.username))

        # Handle keywords.
        keywords = set()

        if json["keywords"]:
            for x in set(json["keywords"]):
                for y in set(list(map(lambda x: x.strip(), x.split(" ")))):
                    if y == "":
                        continue

                    keywords.add(y)

        labels.update(keywords)

        # Handle parent.
        if json["parent"] is not None:
            parent = int(json["parent"].strip("#"))
            labels.add("parent::{}".format(parent))

        # Handle resolution.
        if json["resolution"] is not None:
            if json["resolution"].lower() != "none":
                labels.add("resolution::{}".format(json["resolution"].lower()))

        labels.add("status::{}".format(json["status"].replace("_", "-")))

        # Handle reporter.
        reporter = gitlab_users.get(json["reporter"], tracbot)

        # If reporter is tracbot, we add the original reporter as label.
        if reporter == tracbot:
            labels.add("reporter::{}".format(json["reporter"].strip()))

        project_metadata[project_target_name]["labels"].update(labels)


    moving_tickets_sum = 0

    for name, metadata in project_metadata.items():
        moving_tickets_sum += len(metadata["tickets"])


    # for name, metadata in project_metadata.items():
    #    print("{} ({})".format(name, len(metadata["tickets"])))
    #    print("-" * (len(name) + 6))
    #    print()
    #    for ticket in sorted(metadata["tickets"]):
    #        print("  - {}: {} ({})".format(ticket, ticket_map[ticket]["summary"], ticket_map[ticket]["status"]))
    #    print()

    print("Moving: {}".format(moving_tickets_sum))
    print("Ignored: {}".format(ignored_tickets_sum))

    source_project_name = "legacy/trac" # FIXME(AHF) !!!!
    source_project = gitlab_projects[source_project_name]

    #for project in list(project_metadata.keys()):
    #    if project != "tpo/applications/tor-browser":
    #        del project_metadata[project]

    #pprint.pprint(project_metadata.keys())
    #pprint.pprint(len(project_metadata["tpo/applications/tor-browser"]["tickets"]))

    #project_metadata["tpo/applications/tor-browser"]["tickets"] = set(list(filter(lambda x: x >= 28580, project_metadata["tpo/applications/tor-browser"]["tickets"])))
    #pprint.pprint(len(project_metadata["tpo/applications/tor-browser"]["tickets"]))
    #pprint.pprint(project_metadata["tpo/applications/tor-browser"]["tickets"])

    del project_metadata["tpo/applications/tor-browser"]
    pprint.pprint(project_metadata.keys())

    for name, metadata in dict(sorted(project_metadata.items())).items():
        print("Moving tickets to {} ({} tickets, {} labels, {} milestones)".format(name, len(metadata["tickets"]), len(metadata["labels"]), len(metadata["milestones"])))

        target_project = gitlab_projects[name]

        if True:
            print("Creating labels")
            for label in metadata["labels"]:
                try:
                    target_project.labels.create({'name': label, 'color': '#8899aa'}, sudo="tracbot")
                except gitlab.exceptions.GitlabCreateError as e:
                    if e.response_code == 409:
                        continue

        if True:
            print("Creating milestones")
            for milestone in metadata["milestones"]:
                try:
                    target_project.milestones.create({'title': milestone}, sudo="tracbot")
                except gitlab.exceptions.GitlabCreateError as e:
                    if e.response_code == 409:
                        continue

        # Issues that we want to move. We need to fetch them.
        issues_to_move = {}

        for ticket_id in sorted(metadata["tickets"]):
            print("Fetching {}#{}".format(source_project_name, ticket_id))

            tries = 5

            while tries != 0:
                try:
                    if tries == 1:
                        print("Giving up ...")
                        sys.exit(0)

                    issue = source_project.issues.get(ticket_id)

                    assert issue.iid == ticket_id
                    assert issue.iid not in issues_to_move

                    issues_to_move[issue.iid] = issue

                    # We are good.
                    tries = 0
                except Exception as e:
                    print("Unable to fetch {}: {}".format(ticket_id, e))
                    tries -= 1

        # Move tickets.
        #------------------------------------------------------------------------
        ids = set()
        dummys = set()
        tasks = []

        for issue in sorted(issues_to_move.values(), key=lambda x: x.iid):
            if issue.iid != 1: ## FIXME AHF: 1!!!
                if (issue.iid - 1) not in ids:
                    tasks.append(('dummy', issue.iid - 1))
                    dummys.add(issue.iid - 1)

            ids.add(issue.iid)
            tasks.append(('move', issue.iid))

        for dummy in sorted(dummys):
            tasks.append(('delete', dummy))

        tasks.append(('dummy', 40000))

        dummy_issues = {}

        pprint.pprint(tasks)

        for task, iid in tasks:
            if task == 'dummy':
                print("Creating dummy at {} at {}".format(iid, name))

                tries = 5

                while tries != 0:
                    try:
                        if tries == 1:
                            print("Giving up :-(")
                            sys.exit(1)

                        dummy = target_project.issues.create({
                            'iid': iid,
                            'title': 'Gitlab Migration Milestone',
                            'description': "We're creating this ticket as a part of the Trac-to-Gitlab migration, so that each project's numbering for new tickets will start with 40001.",
                        }, sudo="tracbot")

                        # We are good.
                        tries = 0
                    except Exception as e:
                        print("Unable to create dummy {}: {}".format(iid, e))
                        tries -= 1


                if iid == 40000:
                    # We don't care about closing the others to save one round-trip.
                    try:
                        dummy.state_event = "close"
                        dummy.save(sudo="tracbot")
                    except Exception as e:
                        print("Ugh, unable to close ticket 40k, not important.")

                dummy_issues[iid] = dummy
                continue

            if task == 'delete':
                print("Deleting dummy at {} in {}".format(iid, name))
                assert iid in dummy_issues

                tries = 5

                while tries != 0:
                    try:
                        if tries == 1:
                            print("Giving up :-(")
                            sys.exit(1)

                        dummy_issues[iid].delete(sudo="tracbot")
                        tries = 0
                    except Exception as e:
                        print("Unable to delete dummy {}: {}".format(iid, e))
                        tries -= 1

                continue

            if task == 'move':
                print("Moving {} to {}".format(iid, name))
                assert iid in issues_to_move

                ticket_id = issues_to_move[iid].id

                tries = 5

                while tries != 0:
                    try:
                        if tries == 1:
                            print("Giving up :-(")
                            sys.exit(1)

                        issues_to_move[iid].move(target_project.id, sudo="tracbot")
                        tries = 0
                    except Exception as e:
                        print("Unable to move ticket {} -> {}: {}".format(iid, target_project.id, e))
                        sys.exit(1)
                        tries -= 1

                continue

        print("DONE MIGRATING: {}".format(name))
        target_issues = target_project.issues.list(all=True)
        print("Tickets in destination: {}".format(len(target_issues)))

@cli.command()
@click.pass_context
@click.argument("filename")
def move_tickets(context, filename):
    database = context.obj["database"]
    gitlab_client = context.obj["gitlab_client"]

    groups = {}
    components = set()
    component_to_group = {}
    path_to_group = {}

    with open(filename) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)

        for group in data:
            name = group["group"]
            assert name not in groups

            groups[name] = {}

            for subgroup in group["members"]:
                subgroup_name = subgroup["group"]

                if subgroup_name not in groups[name]:
                    groups[name][subgroup_name] = {}

                for project in subgroup["members"]:
                    project_name = project["project"]

                    assert project_name not in groups[name][subgroup_name]

                    groups[name][subgroup_name][project_name] = set()

                    for trac in project["trac"]:
                        component = trac.strip()

                        groups[name][subgroup_name][project_name].add(component)
                        components.add(component)

                        full_name = "{}/{}/{}".format(name, subgroup_name, project_name)
                        assert component not in component_to_group

                        component_to_group[component] = full_name

                        if full_name not in path_to_group:
                            path_to_group[full_name] = set()

                        path_to_group[full_name].add(component)

    stats = {}
    component_stats = {}
    open_stats = {}
    closed_stats = {}

    click.echo("Going over all tickets to find components ...")
    i = 0
    for ticket_id, json in database.tickets():
        i += 1
        component = json["component"].strip()

        if i % 1000 == 0:
            click.echo(" ... {}".format(i))

        # Bump our counter.
        if component not in component_stats:
            component_stats[component] = 0

        component_stats[component] += 1

        if json["status"] == "closed":
            if component not in closed_stats:
                closed_stats[component] = 0

            closed_stats[component] += 1
        else:
            if component not in open_stats:
                open_stats[component] = 0

            open_stats[component] += 1

    count_sum = 0
    handled_sum = 0
    unhandled_sum = 0

    handled = set()
    unhandled = set()

    for component in component_stats.keys():
        if component in component_to_group:
            handled.add(component)
        else:
            unhandled.add(component)

    print()
    print("Handled Components")
    print("------------------")

    for component in sorted(list(handled)):
        destination = component_to_group[component]
        count = component_stats[component]
        opened_tickets = open_stats.get(component, 0)
        closed_tickets  = closed_stats.get(component, 0)

        count_sum += count
        handled_sum += count

        print("  - '{}' -> '{}' ({} tickets: {} open vs. {} closed)".format(component, destination, count, opened_tickets, closed_tickets))

    print()
    print("Unhandled Components")
    print("--------------------")

    for component in sorted(list(unhandled)):
        count = component_stats[component]
        opened_tickets = open_stats.get(component, 0)
        closed_tickets  = closed_stats.get(component, 0)

        count_sum += count
        unhandled_sum += count

        print("  - '{}' -> 'legacy/trac' ({} tickets: {} open vs. {} closed)".format(component, count, opened_tickets, closed_tickets))

    print()
    print("Stats")
    print("-----")

    print("  - Tickets:         {}".format(count_sum))
    print("  - Tickets moving:  {} ({}%)".format(handled_sum, handled_sum / count_sum * 100))
    print("  - Tickets staying: {} ({}%)".format(unhandled_sum, unhandled_sum / count_sum * 100))
    print()

    # Find TracBot user.
    tracbot = gitlab_client.users.list(username="TracBot")[0]

    gitlab_users = {}

    for u in gitlab_client.users.list(all=True):
        gitlab_users[u.username] = u

    aliases = tor_aliases()

    for alias, ldap in aliases:
        # print("{} alias {}".format(alias, ldap))
        if alias != "micah":
            assert alias not in gitlab_users

        gitlab_users[alias] = gitlab_users[ldap]

    # federico on trac is not federico on ldap
    del gitlab_users["federico"]

    # Check what happens
    gitlab_projects = {}
    for project in gitlab_client.projects.list(all=True):
        assert project.path_with_namespace not in gitlab_projects
        gitlab_projects[project.path_with_namespace] = project

    gitlab_groups = {}
    for group in gitlab_client.groups.list(all=True):
        assert group.full_path not in gitlab_groups
        gitlab_groups[group.full_path] = group

    found_tickets = False

    # Make sure nobody have any tickets.
    for project_path, components in path_to_group.items():
        if project_path not in gitlab_projects:
            print("Need to create: {}".format(project_path))

            tokens = project_path.split("/")

            project_name = tokens[-1]
            group_name   = "/".join(tokens[0:len(tokens) - 1])

            group = gitlab_groups.get(group_name, None)

            if group is None:
                print("Group '{}' missing".format(group_name))
                sys.exit(1)

            logging.info("Creating project: {}".format(project_name))
            project = gitlab_client.projects.create({
                "name": project_name,
                "issues_enabled": True,
                "emails_disabled": True,
                "namespace_id": group.id,
                "visibility": "public",
                "merge_requests_enabled": False,
                "snippets_enabled": False,
            }, sudo="tracbot")

            gitlab_projects[project_path] = project

        project = gitlab_projects[project_path]
        issues = project.issues.list(all=True)

        if len(issues) > 0:
            found_tickets = True
            print("{}: {}".format(project_path, len(issues)))
        else:
            print("{}: N/A".format(project_path))

    if found_tickets:
        print("Projects have tickets... Giving up")
        sys.exit(1)

    # We do a pass to figure out which labels and milestones we need to create in each new project.
    project_metadata = {}
    ignored_tickets_sum = 0

    for ticket_id, json in database.tickets():
        labels = set()

        # Handle component
        if json["component"] is not None:
            labels.add("component::{}".format(json["component"].lower()))

        project_target_name = component_to_group.get(json["component"], None)

        if project_target_name is None:
            ignored_tickets_sum += 1
            continue

        if project_target_name not in project_metadata:
            project_metadata[project_target_name] = {
                "tickets": set([]),
                "milestones": set([]),
                "labels": set([]),
            }

        project_metadata[project_target_name]["tickets"].add(ticket_id)

        # Handle Type.
        assert json["type"] in ["defect", "enhancement", "project", "task"]
        labels.add("type::{}".format(json["type"]))

        # Handle milestone.
        milestone_value = json["milestone"]

        if milestone_value is not None:
            labels.add("milestone::{}".format(milestone_value))
            project_metadata[project_target_name]["milestones"].add(milestone_value)

        # Handle version.
        if json["version"] is not None:
            labels.add("version::{}".format(json["version"].replace(":", "").lower()))

        # Handle priority.
        if json["priority"] is not None:
            labels.add("priority::{}".format(json["priority"].lower()))

        # Handle severity.
        if json["severity"] is not None:
            labels.add("severity::{}".format(json["severity"].lower()))

        # Handle sponsor.
        if json["sponsor"] is not None:
            labels.add("sponsor::{}".format(json["sponsor"][len("Sponsor"):]))

        # Handle points and actual points.
        if json["points"] is not None:
            labels.add("points::{}".format(json["points"]))

        if json["actual_points"] is not None:
            labels.add("actualpoints::{}".format(json["actual_points"]))

        # Handle owner.
        owners = set()

        if json["owner"] is not None:
            for owner in set(list(map(lambda x: x.strip(), json["owner"].split(",")))):
                labels.add("owner::{}".format(owner))

        # Handle reviewer.
        reviewers = set()

        if json["reviewer"]:
            for reviewer in set(json["reviewer"]):
                if reviewer in gitlab_users:
                    reviewers.add(gitlab_users[reviewer])

        # For now we implement reviewer as labels.
        for reviewer in reviewers:
            labels.add("reviewer::{}".format(reviewer.username))

        # Handle keywords.
        keywords = set()

        if json["keywords"]:
            for x in set(json["keywords"]):
                for y in set(list(map(lambda x: x.strip(), x.split(" ")))):
                    if y == "":
                        continue

                    keywords.add(y)

        labels.update(keywords)

        # Handle parent.
        if json["parent"] is not None:
            parent = int(json["parent"].strip("#"))
            labels.add("parent::{}".format(parent))

        # Handle resolution.
        if json["resolution"] is not None:
            if json["resolution"].lower() != "none":
                labels.add("resolution::{}".format(json["resolution"].lower()))

        labels.add("status::{}".format(json["status"].replace("_", "-")))

        # Handle reporter.
        reporter = gitlab_users.get(json["reporter"], tracbot)

        # If reporter is tracbot, we add the original reporter as label.
        if reporter == tracbot:
            labels.add("reporter::{}".format(json["reporter"].strip()))

        project_metadata[project_target_name]["labels"].update(labels)


    moving_tickets_sum = 0

    for name, metadata in project_metadata.items():
        moving_tickets_sum += len(metadata["tickets"])

    print("Moving: {}".format(moving_tickets_sum))
    print("Ignored: {}".format(ignored_tickets_sum))

    source_project_name = "legacy/trac" # FIXME(AHF) !!!!
    source_project = gitlab_projects[source_project_name]

    for name, metadata in dict(sorted(project_metadata.items())).items():
        print("Moving tickets to {} ({} tickets, {} labels, {} milestones)".format(name, len(metadata["tickets"]), len(metadata["labels"]), len(metadata["milestones"])))

        target_project = gitlab_projects[name]

        print("Creating labels")
        for label in metadata["labels"]:
            try:
                target_project.labels.create({'name': label, 'color': '#8899aa'}, sudo="tracbot")
            except gitlab.exceptions.GitlabCreateError as e:
                if e.response_code == 409:
                    continue

        print("Creating milestones")
        for milestone in metadata["milestones"]:
            try:
                target_project.milestones.create({'title': milestone}, sudo="tracbot")
            except gitlab.exceptions.GitlabCreateError as e:
                if e.response_code == 409:
                    continue

        # Issues that we want to move. We need to fetch them.
        issues_to_move = {}

        for ticket_id in sorted(metadata["tickets"]):
            print("Fetching {}#{}".format(source_project_name, ticket_id))

            tries = 5

            while tries != 0:
                try:
                    if tries == 1:
                        print("Giving up ...")
                        sys.exit(0)

                    issue = source_project.issues.get(ticket_id)

                    assert issue.iid == ticket_id
                    assert issue.iid not in issues_to_move

                    issues_to_move[issue.iid] = issue

                    # We are good.
                    tries = 0
                except Exception as e:
                    print("Unable to fetch {}: {}".format(ticket_id, e))
                    tries -= 1

        # Move tickets.
        ids = set()
        dummys = set()
        tasks = []

        for issue in sorted(issues_to_move.values(), key=lambda x: x.iid):
            if issue.iid != 1:
                if (issue.iid - 1) not in ids:
                    tasks.append(('dummy', issue.iid - 1))
                    dummys.add(issue.iid - 1)

            ids.add(issue.iid)
            tasks.append(('move', issue.iid))

        for dummy in sorted(dummys):
            tasks.append(('delete', dummy))

        tasks.append(('dummy', 40000))

        dummy_issues = {}

        pprint.pprint(tasks)

        for task, iid in tasks:
            if task == 'dummy':
                print("Creating dummy at {} at {}".format(iid, name))

                tries = 5

                while tries != 0:
                    try:
                        if tries == 1:
                            print("Giving up :-(")
                            sys.exit(1)

                        dummy = target_project.issues.create({
                            'iid': iid,
                            'title': 'Gitlab Migration Milestone',
                            'description': "We're creating this ticket as a part of the Trac-to-Gitlab migration, so that each project's numbering for new tickets will start with 40001.",
                        }, sudo="tracbot")

                        # We are good.
                        tries = 0
                    except Exception as e:
                        print("Unable to create dummy {}: {}".format(iid, e))
                        tries -= 1


                if iid == 40000:
                    # We don't care about closing the others to save one round-trip.
                    try:
                        dummy.state_event = "close"
                        dummy.save(sudo="tracbot")
                    except Exception as e:
                        print("Ugh, unable to close ticket 40k, not important.")

                dummy_issues[iid] = dummy
                continue

            if task == 'delete':
                print("Deleting dummy at {} in {}".format(iid, name))
                assert iid in dummy_issues

                tries = 5

                while tries != 0:
                    try:
                        if tries == 1:
                            print("Giving up :-(")
                            sys.exit(1)

                        dummy_issues[iid].delete(sudo="tracbot")
                        tries = 0
                    except Exception as e:
                        print("Unable to delete dummy {}: {}".format(iid, e))
                        tries -= 1

                continue

            if task == 'move':
                print("Moving {} to {}".format(iid, name))
                assert iid in issues_to_move

                ticket_id = issues_to_move[iid].id

                tries = 5

                while tries != 0:
                    try:
                        if tries == 1:
                            print("Giving up :-(")
                            sys.exit(1)

                        issues_to_move[iid].move(target_project.id, sudo="tracbot")
                        tries = 0
                    except Exception as e:
                        print("Unable to move ticket {} -> {}: {}".format(iid, target_project.id, e))
                        sys.exit(1)
                        tries -= 1

                continue

        print("DONE MIGRATING: {}".format(name))
        target_issues = target_project.issues.list(all=True)
        print("Tickets in destination: {}".format(len(target_issues)))

@cli.command()
@click.pass_context
def stats_users(context):
    database = context.obj["database"]

    users = {}

    for ticket_id, json in database.tickets():
        reporter = json['reporter']

        if reporter not in users:
            users[reporter] = 0

        users[reporter] += 1

        for change in json['changes']:
            username = change['username']

            if username not in users:
                users[username] = 0

            users[username] += 1

    for username, value in sorted(users.items(), key=lambda x: x[1], reverse=True):
        print("{}".format(username))

@cli.command()
@click.pass_context
@click.argument("project")
@click.argument("start")
@click.argument("stop")
def migrate(context, project, start, stop):
    database = context.obj["database"]
    gitlab_client = context.obj["gitlab_client"]

    project_name = project

    start_ticket_id = int(start)
    stop_ticket_id  = int(stop)

    gitlab_client.auth()

    user = gitlab_client.user
    logging.info("Logged in as: {}".format(user.username))

    account = gitlab_client.users.list(username=user.username)[0]
    logging.info("Account: {}".format(account.username))

    # Handle group creation
    create_group = True
    legacy_group = None

    logging.info("Getting all users")
    group_users = gitlab_client.users.list(all=True)

    logging.info("Listing groups")
    for group in gitlab_client.groups.list(all=True):
        if group.name == "legacy":
            legacy_group = group
            create_group = False
            break

    # Find TracBot user.
    tracbot = gitlab_client.users.list(username="TracBot")[0]

    if create_group:
        logging.info("Creating Group")
        legacy_group = gitlab_client.groups.create({
            "name": "legacy",
            "path": "legacy",
            "description": "Group to handle the import from Trac",
            "emails_disabled": True,
            "visibility": "private",
        }, sudo=tracbot.username)

    members = set(list(map(lambda x: x.id, legacy_group.members.list(all=True))))

    logging.info("Adding members as owners")
    for user in group_users:
        if user.id in members:
            continue

        legacy_group.members.create({
            'user_id': user.id,
            'access_level': gitlab.OWNER_ACCESS,
        })
    # Done handling group creation.

    # Create project.
    project = None

    for p in legacy_group.projects.list():
        if p.name == project_name:
            project = gitlab_client.projects.get(p.id)

    if project is None:
        logging.info("Creating project")
        project = gitlab_client.projects.create({
            "name": project_name,
            "issues_enabled": True,
            "emails_disabled": True,
            "namespace_id": legacy_group.id,
            "visibility": "private",
            "merge_requests_enabled": False,
            "snippets_enabled": False,
        }, sudo=tracbot.username)

    logging.info("Project {} is {}".format(project.name, project.id))

    gitlab_users = {}

    for u in gitlab_client.users.list(all=True):
        gitlab_users[u.username] = u

    aliases = tor_aliases()

    for alias, ldap in aliases:
        # print("{} alias {}".format(alias, ldap))
        if alias != "micah":
            assert alias not in gitlab_users

        gitlab_users[alias] = gitlab_users[ldap]

    # federico on trac is not federico on ldap
    del gitlab_users["federico"]

    # Children and parents.
    children = {}
    parents = {}

    # First state loop
    trac_users = {}
    count = 0

    for ticket_id, json in database.tickets():
        if json["parent"] is not None:
            parent = int(json["parent"].strip("#"))

            if parent not in children:
                children[parent] = set()

            if ticket_id not in parents:
                parents[ticket_id] = set()

            parents[ticket_id].add(parent)
            children[parent].add(ticket_id)

        reporter = json['reporter']

        if reporter not in trac_users:
            trac_users[reporter] = 0

        trac_users[reporter] += 1
        count += 1

        for change in json['changes']:
            username = change['username']

            if username not in trac_users:
                trac_users[username] = 0

            trac_users[username] += 1
            count += 1

    good = open("good.txt", "w")
    bad  = open("bad.txt", "w")

    good_count = 0
    bad_count = 0

    for username, value in sorted(trac_users.items(), key=lambda x: x[1], reverse=True):
        in_gitlab = username in gitlab_users

        if in_gitlab:
            good.write("{};{}\n".format(username, value))
            good_count += value
        else:
            bad.write("{};{}\n".format(username, value))
            bad_count += value

    logging.info("Total: {} events".format(count))
    logging.info("In:    {} ({}%) events saved from Trac users".format(good_count, good_count / count * 100))
    logging.info("Out:   {} ({}%) events will be from TracBot".format(bad_count, bad_count / count * 100))

    # Fetch all Milestones.
    milestones = {}

    for m in project.milestones.list(all=True):
        milestones[m.title] = m

    # Tickets we have migrated.
    tickets_migrated = {}

    # Begin the actual migration.
    for ticket_id, json in database.tickets():
        labels = set()

        # Handle Type.
        assert json["type"] in ["defect", "enhancement", "project", "task"]
        labels.add("type::{}".format(json["type"]))

        # Handle milestone.
        milestone_value = json["milestone"]
        milestone = None

        if milestone_value is not None:
            labels.add("milestone::{}".format(milestone_value))

            if milestone_value in milestones:
                milestone = milestones[milestone_value]
            else:
                milestone = project.milestones.create({
                    'title': milestone_value,
                }, sudo=tracbot.username)

                milestones[milestone_value] = milestone

        # Handle version.
        if json["version"] is not None:
            labels.add("version::{}".format(json["version"].replace(":", "").lower()))

        # Handle component
        if json["component"] is not None:
            labels.add("component::{}".format(json["component"].lower()))

        # Handle priority.
        if json["priority"] is not None:
            labels.add("priority::{}".format(json["priority"].lower()))

        # Handle severity.
        if json["severity"] is not None:
            labels.add("severity::{}".format(json["severity"].lower()))

        # Handle sponsor.
        if json["sponsor"] is not None:
            labels.add("sponsor::{}".format(json["sponsor"][len("Sponsor"):]))

        # Handle points and actual points.
        points = None
        actual_points = None

        if json["points"] is not None:
            labels.add("points::{}".format(json["points"]))
            points = convert_point2(json["points"])

        if json["actual_points"] is not None:
            labels.add("actualpoints::{}".format(json["actual_points"]))
            actual_points = convert_point2(json["actual_points"])

        # Handle owner.
        owners = set()

        if json["owner"] is not None:
            for owner in set(list(map(lambda x: x.strip(), json["owner"].split(",")))):
                labels.add("owner::{}".format(owner))

                if owner in gitlab_users:
                    owners.add(gitlab_users[owner])

        # Handle reviewer.
        reviewers = set()

        if json["reviewer"]:
            for reviewer in set(json["reviewer"]):
                if reviewer in gitlab_users:
                    reviewers.add(gitlab_users[reviewer])

        # For now we implement reviewer as labels.
        for reviewer in reviewers:
            labels.add("reviewer::{}".format(reviewer.username))

        # Handle CC.
        cc = set()

        if json["cc"]:
            for m in set(json["cc"]):
                if m in gitlab_users:
                    cc.add(gitlab_users[m])

        # Handle keywords.
        keywords = set()

        if json["keywords"]:
            for x in set(json["keywords"]):
                for y in set(list(map(lambda x: x.strip(), x.split(" ")))):
                    if y == "":
                        continue

                    keywords.add(y)

        labels.update(keywords)

        # Handle parent.
        if json["parent"] is not None:
            parent = int(json["parent"].strip("#"))
            labels.add("parent::{}".format(parent))

        # Handle resolution.
        if json["resolution"] is not None:
            if json["resolution"].lower() != "none":
                labels.add("resolution::{}".format(json["resolution"].lower()))

        labels.add("status::{}".format(json["status"].replace("_", "-")))

        # Handle reporter.
        reporter = gitlab_users.get(json["reporter"], tracbot)

        # If reporter is tracbot, we add the original reporter as label.
        if reporter == tracbot:
            labels.add("reporter::{}".format(json["reporter"].strip()))

        # Handle summary.
        summary = json["summary"]

        # FIXME(ahf): Happens for 8562, 30499, 31728, but all of them seems
        # less important.
        if len(summary) > 255:
            summary = summary[0:255]

        description = convert(json["description"], "uploads", multilines=False)

        if reporter == tracbot:
            description += "\n\n**Trac**:  \n"
            description += "**Username**: {}  \n".format(json["reporter"])

        # Handle attachments.
        attachments = []

        for attachment in json["attachments"]:
            path = "data/attachments/{}/{}".format(ticket_id, attachment["filename"])

            if not os.path.exists(path):
                continue

            uploader = gitlab_users.get(attachment["uploader"], tracbot)
            content = open("data/attachments/{}/{}".format(ticket_id, attachment["filename"]), "rb").read()
            date = attachment["date"]
            filename = attachment["filename"]

            if len(filename) > 128:
                assert ticket_id == 16887
                filename = "trac-convert-long-filename.diff"

            attachments.append({
                "filename": filename,
                "content": content,
                "date": date,
                "uploader": uploader,
                "trac_username": attachment["uploader"],
            })

        # Handle changelog.
        events = []

        for change in json["changes"]:
            timestamp = change["timestamp"]
            change_user = change["username"]

            change_type = change["type"]
            old_value = change["old_value"]
            new_value = change["new_value"]

            events.append({
                "timestamp": timestamp,
                "user": change_user,
                "type": change_type,
                "old_value": old_value,
                "new_value": new_value,
            })

        # Convert events into grouped changes entries.
        changes = []

        for key, events in itertools.groupby(events, lambda event: (event["user"], event["timestamp"])):
            change_user, timestamp = key
            events = list(events)

            changes.append(cleanup_event({
                "user": change_user,
                "timestamp": timestamp,
                "events": events,
            }))

        # Changelog, finally!
        changelog = []

        for change in changes:
            change_user = gitlab_users.get(change["user"], tracbot)

            body = change_to_note(change, change_user == tracbot)
            assert body is not None

            changelog.append({
                "user": change_user,
                "timestamp": change["timestamp"],
                "body": body,
            })

        if ticket_id < start_ticket_id:
            logging.info("Skipping #{} (start) for Gitlab user {} (Trac User: {}) (Labels: {})".format(ticket_id, reporter.username, json["reporter"], labels))
            continue

        if ticket_id > stop_ticket_id:
            logging.info("Skipping #{} (stop) for Gitlab user {} (Trac User: {}) (Labels: {})".format(ticket_id, reporter.username, json["reporter"], labels))
            continue

        logging.info("Migrating #{} for Gitlab user {} (Trac User: {}) (Labels: {})".format(ticket_id, reporter.username, json["reporter"], labels))

        if True:
            tries = 3

            while tries != 0:
                try:
                    issue_content = {
                        "iid": ticket_id,
                        "title": summary,
                        "description": description,
                        "created_at": json["opened"],
                        "labels": labels,
                    }

                    if milestone is not None:
                        assert milestone.id
                        issue_content["milestone_id"] = milestone.id

                    if owners:
                        issue_content["assignee_ids"] = list(map(lambda x: x.id, owners))

                    issue = project.issues.create(issue_content, sudo=reporter.username)

                    # Upload attachments.
                    for attachment in attachments:
                        uploaded_file = project.upload(attachment["filename"], filedata=attachment["content"])

                        attachment_body = "**Trac**:  \n"

                        if attachment["uploader"] == tracbot:
                            attachment_body += "**Username**: {}  \n\n".format(attachment["trac_username"])

                        attachment_body += uploaded_file["markdown"]

                        issue.notes.create({
                            "body": attachment_body,
                            "created_at": attachment["date"],
                        }, sudo=attachment["uploader"].username)

                    # Create changelog
                    for change in changelog:
                        if change["body"] == "":
                            continue

                        if ticket_id == 2820 and len(change["body"]) > 2048:
                            change["body"] = change["body"][0:2048] + " ... (trimmed from Trac conversion)"

                        issue.notes.create({
                            "body": change["body"],
                            "created_at": change["timestamp"],
                        }, sudo=change["user"].username)

                    # Set estimates, if any.
                    if points is not None:
                        print(points)
                        issue.time_estimate(points, sudo=tracbot.username)

                    if actual_points is not None:
                        issue.add_spent_time(actual_points, sudo=tracbot.username)

                    # Add CC's.
                    for subscriber in cc:
                        try:
                            issue.subscribe(sudo=subscriber.username)
                        except Exception as e:
                            pass

                    # Close the issue.
                    if json["status"] == "closed":
                        issue.state_event = "close"
                        issue.updated_at = json["last_modified"]
                        issue.save(sudo=tracbot.username)

                    # We are good.
                    tries = 0
                    tickets_migrated[ticket_id] = issue
                except GitlabCreateError as e:
                    if e.response_code == 409:
                        print("Deleting issue {}".format(ticket_id))
                        project.issues.delete(ticket_id)

                    tries = tries - 1

                    print("GitlabCreateError: {}".format(e))

                    if tries == 0:
                        raise e
                except Exception as e:
                    tries = tries - 1

                    print("Exception: {}".format(e))

                    if tries == 0:
                        raise e

    if len(tickets_migrated.keys()) > 0:
        logging.info("Adding parent/child metadata")

        for issue_id, issue in dict(sorted(tickets_migrated.items())).items():
            post = False
            description = "**Trac**:  \n"

            our_children = children.get(issue_id, set())
            our_parents = parents.get(issue_id, set())
            assert len(our_parents) <= 1

            if len(our_parents) == 1:
                post = True
                description += "**Parent Ticket**: #{}  \n".format(list(our_parents)[0])

            if len(our_children) > 0:
                post = True
                description += "**Child Ticket(s)**: {}  \n".format(", ".join(list(map(lambda x: "#{}".format(x), our_children))))

            if post:
                logging.info("Adding parent/child info: #{}".format(issue_id))

                issue.notes.create({
                    "body": description,
                    "created_at": issue.created_at,
                }, sudo=tracbot.username)

    logging.info("Cleaning up group")
    members = legacy_group.members.list(all=True)

    for member in members:
        if member.username == "ahf-admin":
            continue

        member.delete()

def cleanup_event(event):
    # FIXME(ahf): Can multiple users collide in our groupby()? We sure hope not.
    assert all(map(lambda x: x["user"] == event["user"], event["events"]))

    cleaned_event = {}

    for e in event["events"]:
        if e["type"] == "comment":
            # Comments with no value can be removed.
            if e["new_value"].strip() == "":
                continue

            cleaned_event["comment"] = e["new_value"]
        elif e["type"] == "keywords":
            old = set(map(lambda x: x.strip(), e["old_value"].split(",")))
            new = set(map(lambda x: x.strip(), e["new_value"].split(",")))

            cleaned_event["keywords"] = {
                "old": old.difference(new),
                "new": new.difference(old),
            }
        else:
            cleaned_event[e["type"]] = {
                "old": e["old_value"],
                "new": e["new_value"],
            }

    r = {
        "user":  event["user"],
        "timestamp": event["timestamp"],
        "changes":   cleaned_event,
    }

    return r

def change_to_note(change, is_tracbot):
    # Let's see if there is a comment and extract that first.
    comment = ""

    if "comment" in change["changes"]:
        comment = convert(change["changes"]["comment"], "uploads", multilines=False) # FIXME(ahf): Upload?
        del change["changes"]["comment"]

    metadata_changed = False

    if is_tracbot:
        metadata = "**Username**: {}  \n".format(change["user"])
        metadata_changed = True
    else:
        metadata = ""

    for key, value in change["changes"].items():
        if key == "attachment":
            continue

        if key.startswith("_comment"):
            continue

        metadata_changed = True

        old = value["old"]
        new = value["new"]

        to = "**to**"
        end = ""


        if key == "description":
            to = '\n\n**to**\n\n'

            old = convert(old, "uploads", multilines=False)
            new = convert(new, "uploads", multilines=False)

        if key == "keywords":
            to = "**deleted**, "
            end = "**added**"

            if type(old) == set:
                old = ", ".join(old)

            if type(new) == set:
                new = ", ".join(new)

        if old is None or old is "":
            old = "*N/A*"

        if new is None or new is "":
            new = "*N/A*"

        metadata += "**{}**: {} {} {} {}  \n".format(key.capitalize(), old, to, new, end)


    body = comment

    if metadata_changed:
        body += "\n\n**Trac**:  \n" + metadata

    return body

def convert_point2(x):
    if x is None:
        return None

    try:
        x = float(x)
    except Exception as e:
        return None

    if x <= 0.0:
        return None

    if int(x * 480) % 60 == 0:
        v = int(x * 8)

        if v == 0:
            return None

        return "{}h".format(v)
    else:
        return "{0}h {1}m".format(int(x * 8), int(x * 480) % 60)

@cli.command()
@click.pass_context
@click.argument("project")
def verify(context, project):
    database = context.obj["database"]
    gitlab_client = context.obj["gitlab_client"]

    project_name = project

    gitlab_client.auth()

    user = gitlab_client.user
    logging.info("Logged in as: {}".format(user.username))

    account = gitlab_client.users.list(username=user.username)[0]
    logging.info("Account: {}".format(account.username))


    # Create project.
    project = None

    for p in gitlab_client.projects.list():
        if p.path_with_namespace == project_name:
            project = gitlab_client.projects.get(p.id)

    if project is None:
        logging.info("Project not found")
        sys.exit(1)

    logging.info("Project {} is {}".format(project.name, project.id))

    gitlab_users = {}

    for u in gitlab_client.users.list(all=True):
        gitlab_users[u.username] = u

    aliases = tor_aliases()

    for alias, ldap in aliases:
        # print("{} alias {}".format(alias, ldap))
        if alias != "micah":
            assert alias not in gitlab_users

        gitlab_users[alias] = gitlab_users[ldap]

    # federico on trac is not federico on ldap
    del gitlab_users["federico"]

    # Children and parents.
    children = {}
    parents = {}

    # First state loop
    trac_users = {}
    count = 0

    for ticket_id, json in database.tickets():
        if json["parent"] is not None:
            parent = int(json["parent"].strip("#"))

            if parent not in children:
                children[parent] = set()

            if ticket_id not in parents:
                parents[ticket_id] = set()

            parents[ticket_id].add(parent)
            children[parent].add(ticket_id)

        reporter = json['reporter']

        if reporter not in trac_users:
            trac_users[reporter] = 0

        trac_users[reporter] += 1
        count += 1

        for change in json['changes']:
            username = change['username']

            if username not in trac_users:
                trac_users[username] = 0

            trac_users[username] += 1
            count += 1

    # Find TracBot user.
    tracbot = gitlab_client.users.list(username="TracBot")[0]

    not_found = 0

    # Fetch all Milestones.
    milestones = {}

    for m in project.milestones.list(all=True):
        milestones[m.title] = m

    # Tickets we have migrated.
    tickets_migrated = set()

    min_ticket_id = float("Inf")
    max_ticket_id = -1

    # Begin the actual migration.
    for ticket_id, json in database.tickets():
        labels = set()
        tickets_migrated.add(ticket_id)

        if ticket_id > max_ticket_id:
            max_ticket_id = ticket_id

        if ticket_id < min_ticket_id:
            min_ticket_id = ticket_id

        # Handle Type.
        assert json["type"] in ["defect", "enhancement", "project", "task"]
        labels.add("type::{}".format(json["type"]))

        # Handle milestone.
        milestone_value = json["milestone"]

        if milestone_value is not None:
            labels.add("milestone::{}".format(milestone_value))

        # Handle version.
        if json["version"] is not None:
            labels.add("version::{}".format(json["version"].replace(":", "").lower()))

        # Handle component
        if json["component"] is not None:
            labels.add("component::{}".format(json["component"].lower()))

        # Handle priority.
        if json["priority"] is not None:
            labels.add("priority::{}".format(json["priority"].lower()))

        # Handle severity.
        if json["severity"] is not None:
            labels.add("severity::{}".format(json["severity"].lower()))

        # Handle sponsor.
        if json["sponsor"] is not None:
            labels.add("sponsor::{}".format(json["sponsor"][len("Sponsor"):]))

        # Handle points and actual points.
        points = None
        actual_points = None

        if json["points"] is not None:
            labels.add("points::{}".format(json["points"]))
            points = convert_point2(json["points"])

        if json["actual_points"] is not None:
            labels.add("actualpoints::{}".format(json["actual_points"]))
            actual_points = convert_point2(json["actual_points"])

        # Handle owner.
        owners = set()

        if json["owner"] is not None:
            for owner in set(list(map(lambda x: x.strip(), json["owner"].split(",")))):
                labels.add("owner::{}".format(owner))

                if owner in gitlab_users:
                    owners.add(gitlab_users[owner])

        # Handle reviewer.
        reviewers = set()

        if json["reviewer"]:
            for reviewer in set(json["reviewer"]):
                if reviewer in gitlab_users:
                    reviewers.add(gitlab_users[reviewer])

        # For now we implement reviewer as labels.
        for reviewer in reviewers:
            labels.add("reviewer::{}".format(reviewer.username))

        # Handle CC.
        cc = set()

        if json["cc"]:
            for m in set(json["cc"]):
                if m in gitlab_users:
                    cc.add(gitlab_users[m])

        # Handle keywords.
        keywords = set()

        if json["keywords"]:
            for x in set(json["keywords"]):
                for y in set(list(map(lambda x: x.strip(), x.split(" ")))):
                    if y == "":
                        continue

                    keywords.add(y)

        labels.update(keywords)

        # Handle parent.
        if json["parent"] is not None:
            parent = int(json["parent"].strip("#"))
            labels.add("parent::{}".format(parent))

        # Handle resolution.
        if json["resolution"] is not None:
            if json["resolution"].lower() != "none":
                labels.add("resolution::{}".format(json["resolution"].lower()))

        labels.add("status::{}".format(json["status"].replace("_", "-")))

        # Handle reporter.
        reporter = gitlab_users.get(json["reporter"], tracbot)

        # If reporter is tracbot, we add the original reporter as label.
        if reporter == tracbot:
            labels.add("reporter::{}".format(json["reporter"].strip()))

        # Handle summary.
        summary = json["summary"]

        # FIXME(ahf): Happens for 8562, 30499, 31728, but all of them seems
        # less important.
        if len(summary) > 255:
            summary = summary[0:255]

        description = convert(json["description"], "uploads", multilines=False)

        if reporter == tracbot:
            description += "\n\n**Trac**:  \n"
            description += "**Username**: {}  \n".format(json["reporter"])

        # Handle attachments.
        attachments = []

        for attachment in json["attachments"]:
            path = "data/attachments/{}/{}".format(ticket_id, attachment["filename"])

            if not os.path.exists(path):
                continue

            uploader = gitlab_users.get(attachment["uploader"], tracbot)
            content = open("data/attachments/{}/{}".format(ticket_id, attachment["filename"]), "rb").read()
            date = attachment["date"]
            filename = attachment["filename"]

            if len(filename) > 128:
                assert ticket_id == 16887
                filename = "trac-convert-long-filename.diff"

            attachments.append({
                "filename": filename,
                "content": content,
                "date": date,
                "uploader": uploader,
                "trac_username": attachment["uploader"],
            })

        # Handle changelog.
        events = []

        for change in json["changes"]:
            timestamp = change["timestamp"]
            change_user = change["username"]

            change_type = change["type"]
            old_value = change["old_value"]
            new_value = change["new_value"]

            events.append({
                "timestamp": timestamp,
                "user": change_user,
                "type": change_type,
                "old_value": old_value,
                "new_value": new_value,
            })

        # Convert events into grouped changes entries.
        changes = []

        for key, events in itertools.groupby(events, lambda event: (event["user"], event["timestamp"])):
            change_user, timestamp = key
            events = list(events)

            changes.append(cleanup_event({
                "user": change_user,
                "timestamp": timestamp,
                "events": events,
            }))

        # Changelog, finally!
        changelog = []

        for change in changes:
            change_user = gitlab_users.get(change["user"], tracbot)

            body = change_to_note(change, change_user == tracbot)
            assert body is not None

            changelog.append({
                "user": change_user,
                "timestamp": change["timestamp"],
                "body": body,
            })

        if ticket_id % 1000 == 0:
            logging.info(" ... {}".format(ticket_id))

        try:
            issue = project.issues.get(ticket_id)

            print("Found: {}".format(ticket_id))

            if issue.author["id"] != reporter.id:
                print("Author delta in #{}".format(ticket_id))

            # Check summary.
            if issue.title != summary.strip():
                print("Summary delta in #{}: '{}' vs. '{}'".format(ticket_id, issue.title, summary.strip()))

            # Check description.
            if description.strip() not in issue.description:
                print("Description delta in #{}".format(ticket_id))

            # Check labels.
            if labels != set(issue.labels):
                a = set(issue.labels) - labels
                b = labels - set(issue.labels)
                print("Label delta in #{}: {} vs. {}".format(ticket_id, a, b))

        except gitlab.exceptions.GitlabGetError as e:
            if e.response_code == 404:
                not_found += 1
                # logging.error("Unable to get ticket #{}".format(ticket_id))
                continue

    # logging.info("Checking if missing tickets are indeed missing ({} -> {})".format(min_ticket_id, max_ticket_id + 1))
    print("Not found: {}".format(not_found))

    #for ticket_id in range(min_ticket_id, max_ticket_id + 1):
    #    if ticket_id in tickets_migrated:
    #        continue

    #    exists = True

    #    try:
    #        project.issues.get(ticket_id)
    #    except gitlab.exceptions.GitlabGetError as e:
    #        if e.response_code == 404:
    #            exists = False

    #    if exists:
    #        logging.info("Missing ticket exists #{}".format(ticket_id))


@cli.command()
@click.pass_context
def sync(context):
    sync_tickets(context)
    sync_attachments(context)
    sync_wiki(context)

@cli.command()
@click.pass_context
@click.argument("project")
def label_stats(context, project):
    click.echo(project)

    label_stats = {}

    database = context.obj["database"]
    gitlab_client = context.obj["gitlab_client"]

    gitlab_client.auth()

    user = gitlab_client.user
    logging.info("Logged in as: {}".format(user.username))

    account = gitlab_client.users.list(username=user.username)[0]
    logging.info("Account: {}".format(account.username))

    gitlab_users = {}

    for u in gitlab_client.users.list(all=True):
        gitlab_users[u.username] = u

    aliases = tor_aliases()

    for alias, ldap in aliases:
        # print("{} alias {}".format(alias, ldap))
        if alias != "micah":
            assert alias not in gitlab_users

        gitlab_users[alias] = gitlab_users[ldap]

    # federico on trac is not federico on ldap
    del gitlab_users["federico"]

    # Children and parents.
    children = {}
    parents = {}

    # First state loop
    trac_users = {}
    count = 0

    for ticket_id, json in database.tickets():
        if json["parent"] is not None:
            parent = int(json["parent"].strip("#"))

            if parent not in children:
                children[parent] = set()

            if ticket_id not in parents:
                parents[ticket_id] = set()

            parents[ticket_id].add(parent)
            children[parent].add(ticket_id)

        reporter = json['reporter']

        if reporter not in trac_users:
            trac_users[reporter] = 0

        trac_users[reporter] += 1
        count += 1

        for change in json['changes']:
            username = change['username']

            if username not in trac_users:
                trac_users[username] = 0

            trac_users[username] += 1
            count += 1

    # Find TracBot user.
    tracbot = gitlab_client.users.list(username="TracBot")[0]

    # Fetch all Milestones.
    milestones = {}

    # Tickets we have migrated.
    tickets_migrated = set()

    min_ticket_id = float("Inf")
    max_ticket_id = -1

    # Begin the actual migration.
    for ticket_id, json in database.tickets():
        labels = set()
        tickets_migrated.add(ticket_id)

        if ticket_id > max_ticket_id:
            max_ticket_id = ticket_id

        if ticket_id < min_ticket_id:
            min_ticket_id = ticket_id

        # Handle Type.
        assert json["type"] in ["defect", "enhancement", "project", "task"]
        labels.add("type::{}".format(json["type"]))

        # Handle milestone.
        milestone_value = json["milestone"]

        if milestone_value is not None:
            labels.add("milestone::{}".format(milestone_value))

        # Handle version.
        if json["version"] is not None:
            labels.add("version::{}".format(json["version"].replace(":", "").lower()))

        # Handle component
        if json["component"] is not None:
            labels.add("component::{}".format(json["component"].lower()))

        # Handle priority.
        if json["priority"] is not None:
            labels.add("priority::{}".format(json["priority"].lower()))

        # Handle severity.
        if json["severity"] is not None:
            labels.add("severity::{}".format(json["severity"].lower()))

        # Handle sponsor.
        if json["sponsor"] is not None:
            labels.add("sponsor::{}".format(json["sponsor"][len("Sponsor"):]))

        # Handle points and actual points.
        points = None
        actual_points = None

        if json["points"] is not None:
            labels.add("points::{}".format(json["points"]))
            points = convert_point2(json["points"])

        if json["actual_points"] is not None:
            labels.add("actualpoints::{}".format(json["actual_points"]))
            actual_points = convert_point2(json["actual_points"])

        # Handle owner.
        owners = set()

        if json["owner"] is not None:
            for owner in set(list(map(lambda x: x.strip(), json["owner"].split(",")))):
                labels.add("owner::{}".format(owner))

                if owner in gitlab_users:
                    owners.add(gitlab_users[owner])

        # Handle reviewer.
        reviewers = set()

        if json["reviewer"]:
            for reviewer in set(json["reviewer"]):
                if reviewer in gitlab_users:
                    reviewers.add(gitlab_users[reviewer])

        # For now we implement reviewer as labels.
        for reviewer in reviewers:
            labels.add("reviewer::{}".format(reviewer.username))

        # Handle CC.
        cc = set()

        if json["cc"]:
            for m in set(json["cc"]):
                if m in gitlab_users:
                    cc.add(gitlab_users[m])

        # Handle keywords.
        keywords = set()

        if json["keywords"]:
            for x in set(json["keywords"]):
                for y in set(list(map(lambda x: x.strip(), x.split(" ")))):
                    if y == "":
                        continue

                    keywords.add(y)

        labels.update(keywords)

        # Handle parent.
        if json["parent"] is not None:
            parent = int(json["parent"].strip("#"))
            labels.add("parent::{}".format(parent))

        # Handle resolution.
        if json["resolution"] is not None:
            if json["resolution"].lower() != "none":
                labels.add("resolution::{}".format(json["resolution"].lower()))

        labels.add("status::{}".format(json["status"].replace("_", "-")))

        # Handle reporter.
        reporter = gitlab_users.get(json["reporter"], tracbot)

        # If reporter is tracbot, we add the original reporter as label.
        if reporter == tracbot:
            labels.add("reporter::{}".format(json["reporter"].strip()))

        if ticket_id % 1000 == 0:
            logging.info(" ... {}".format(ticket_id))

        for label in labels:
            if label not in label_stats:
                label_stats[label] = 0

            label_stats[label] += 1

    for label in sorted(label_stats, key=label_stats.get, reverse=True):
        print("  - {}: {}".format(label, label_stats[label]))

    print()
    print("Total labels: {}".format(len(label_stats.keys())))

@cli.command()
@click.pass_context
def browser_dump(context):
    database = context.obj["database"]
    tickets = {}

    ticket_map = {}

    for ticket_id, json in database.tickets():
        component = json["component"]

        if component != "Applications/Tor Browser":
            continue

        if json["status"] != "closed":
            continue

        found = False
        target = "MISSING-GL-TAG"

        for kw in json["keywords"]:
            if kw.lower().startswith("gitlab-tb"):
                assert not found

                target = kw.lower()
                found = True

        if target not in tickets:
            tickets[target] = set()

        tickets[target].add(ticket_id)
        ticket_map[ticket_id] = json

    count = 0

    for target, issues in tickets.items():
        print("{}".format(target))
        print("{}".format("-" * len(target)))
        print()

        for issue_id in sorted(list(issues)):
            print("  - {}: {}".format(issue_id, ticket_map[issue_id]["summary"]))
            count += 1

        print()

    print("Total ticket count: {}".format(count))


def sync_tickets(context):
    trac_client = context.obj["trac_client"]
    database = context.obj["database"]

    # FIXME(ahf): Move to parameter.
    max_issue_id = 35000
    known_tickets = set()

    for ticket_id, json in database.tickets():
        known_tickets.add(ticket_id)

    for i in range(1, max_issue_id):
        if i in known_tickets:
            print("Skipping {}".format(i))
            continue

        try:
            ticket = trac_client.fetch_ticket(i)
        except Exception as e:
            logging.error("Error fetching {}".format(i))
            continue

        try:
            database.create_ticket(i, ticket)
        except Exception as e:
            print("Ugh, unable to create - duplicate? :-(")
            continue

def sync_attachments(context):
    trac_client = context.obj["trac_client"]
    database = context.obj["database"]

    logging.info("Syncing attachments")

    for ticket_id, json in database.tickets():
        attachments = json["attachments"]

        if len(attachments) == 0:
            continue

        for attachment in attachments:
            try:
                trac_client.fetch_attachment(ticket_id, attachment["filename"])
            except Exception as e:
                print("Unable to fetch file {}: {}".format(attachment["filename"], e))
                continue


@cli.command()
@click.pass_context
@click.argument("filename")
def convert_wiki(context, filename):
    print(process_one_wiki_page("wikis/", filename))

def process_one_wiki_page(path, filename):
    full_filename = filename

    if filename.startswith(path):
        filename = filename[len(path):-1]

    body = ""

    with open(full_filename) as f:
        for line in f:
            # Empty lines stay as is.
            if line.strip() == "":
                body += line
                continue

            line = line.rstrip()
            body += line + '\n'

    return convert(body, os.path.dirname('/wikis/{}'.format(filename)))

@cli.command()
@click.pass_context
def sync_wiki(context):
    trac_client = context.obj["trac_client"]
    database = context.obj["database"]

    prefix = "data/wiki-data/"

    os.makedirs(prefix, exist_ok=True)

    for page in trac_client.wiki_pages():
        info = trac_client.fetch_wiki_page_info(page)

        # Create the page.
        wiki_page = trac_client.fetch_wiki_page(page, info["version"])
        # database.create_wiki_page(info["name"], info["author"], info["version"], info["lastModified"], info["comment"], wiki_page)

        # Get the attachments.
        for attachment in trac_client.wiki_attachments(page):
            attachment_dir = os.path.dirname(page + ".md")
            attachment_dest = os.path.basename(attachment)

            filename  = os.path.join(prefix, attachment_dir, attachment_dest)

            print(attachment_dir)
            print(attachment_dest)
            print("XXX FILENAME: {}".format(filename))
            assert not os.path.exists(filename)

            content = trac_client.fetch_wiki_attachment(attachment)

            print("MKDIRS: {}".format(os.path.dirname(filename)))
            os.makedirs(os.path.dirname(filename), exist_ok=True)

            with open(filename, "wb") as f:
                f.write(content)

        # Write the markdown file to disk.
        if page == "WikiStart":
            title = "home"
        else:
            title = page

        directory = os.path.dirname(os.path.join(prefix, title))
        filename = os.path.join(directory, os.path.basename(title) + ".md")

        os.makedirs(directory, exist_ok=True)

        with open(filename, "w") as f:
            f.write(wiki_page)

def main():
    cli(obj={})


#def main(config_file: str) -> None:
    #    trac_client.methods()

    #for name, author, content in database.wiki_pages():
    #    print(convert(content, os.path.dirname("/wikis/{}".format(name))))
