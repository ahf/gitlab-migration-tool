# Copyright (c) 2019-2020 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import datetime
import json
import sqlite3

class Database:
    def __init__(self, filename: str) -> None:
        self._connection = sqlite3.connect(filename)
        cursor = self._connection.cursor()

        # Create table for Wiki entries.
        cursor.execute("CREATE TABLE IF NOT EXISTS wiki (name TEXT, author TEXT, version INTEGER, last_modified DATETIME, comment TEXT, content TEXT)")
        cursor.execute("CREATE UNIQUE INDEX IF NOT EXISTS index_wiki ON wiki (name)")

        # Create table for ticket entries.
        cursor.execute("CREATE TABLE IF NOT EXISTS tickets (id INTEGER, json TEXT)")
        cursor.execute("CREATE UNIQUE INDEX IF NOT EXISTS index_tickets ON tickets (id)")

        # Commit the changes.
        self._connection.commit()

    def create_wiki_page(self, name: str, author: str, version: int, last_modified: str, comment: str, content: str) -> None:
        self._connection.execute("INSERT INTO wiki (name, author, version, last_modified, comment, content) VALUES (?, ?, ?, ?, ?, ?)", (name, author, version, last_modified, comment, content))
        self._connection.commit()

    def wiki_pages(self):
        c = self._connection.cursor()
        c.execute("SELECT name, author, content FROM wiki")
        return c.fetchall()

    def tickets(self):
        c = self._connection.cursor()
        c.execute("SELECT id, json FROM tickets")
        return map(lambda x: (x[0], json.loads(x[1])), c.fetchall())

    def create_ticket(self, issue_id, content):
        self._connection.execute("INSERT INTO tickets (id, json) VALUES (?, ?)", (issue_id, json.dumps(content, default=json_datetime)))
        self._connection.commit()

def json_datetime(obj):
    if isinstance(obj, datetime.datetime):
        return obj.isoformat()

    raise TypeError ("Type %s not serializable" % type(obj))
