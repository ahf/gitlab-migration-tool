# Copyright (c) 2019-2020 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import configparser
import logging


class Config:
    def __init__(self, filename: str) -> None:
        self._config = configparser.ConfigParser()

        # Set the default values.
        self._config["general"] = {
            "log_level": "info",
            "database": "storage.db",
        }

        self._config["trac"] = {
            "url": "",
            "username": "",
            "password": "",
        }

        self._config["gitlab"] = {
            "url": "",
            "private_token": "",
        }

        # Load the configuration file and override our default values.
        self._config.read(filename)

    def log_level(self) -> int:
        log_level = self._config["general"]["log_level"]
        numeric_log_level = getattr(logging, log_level.upper(), None)

        if not isinstance(numeric_log_level, int):
            raise Exception("Invalid log level: {}".format(log_level))

        return numeric_log_level

    def database(self) -> str:
        return self._config["general"]["database"]

    def trac_url(self) -> str:
        return self._config["trac"]["url"]

    def trac_username(self) -> str:
        return self._config["trac"]["username"]

    def trac_password(self) -> str:
        return self._config["trac"]["password"]

    def gitlab_url(self) -> str:
        return self._config["gitlab"]["url"]

    def gitlab_private_token(self) -> str:
        return self._config["gitlab"]["private_token"]
